var minify = require('gulp-js-minify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');

module.exports = function (gulp, plugins) {
    return function () {
        gulp.src('./src/js/*.js')
            .pipe(minify())
            .pipe(concat('script.js'))
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(gulp.dest('./dist/js'));
    };
};