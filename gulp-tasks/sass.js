var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');

module.exports = function (gulp, plugins) {
    return function () {
        gulp.src('./src/scss/**/*.scss')
            .pipe(sass())
            .pipe(cleanCSS())
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(gulp.dest('./dist/css'));
    };
};