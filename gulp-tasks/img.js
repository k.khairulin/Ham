var imagemin = require('gulp-imagemin');

module.exports = function (gulp, plugins) {
    return function () {
        gulp.src('./src/img/**/*.*')
            .pipe(imagemin())
            .pipe(gulp.dest('./dist/img'));
    };
};