var browserSync = require('browser-sync').create();

const reload = browserSync.reload;

module.exports = function (gulp, plugins) {
    return function () {
        browserSync.init({
            server: './'
        });

        gulp.watch('./src/scss/**/*.{sass,scss}', ['sass']).on('change', reload);
        gulp.watch('./index.html').on('change', reload);
        gulp.watch('./src/img/**/*.*', ['img']).on('change', reload);
        gulp.watch('./src/js/*.js', ['scripts']).on('change', reload);
    };
};